from helpers.application_viewsets import ApplicationViewSet
from rest_framework.generics import RetrieveUpdateAPIView, ListAPIView
from rest_framework.viewsets import ViewSet
from courses.permissions import SupervisorPermission, IsAuthenticated, CoursePermission, AssistantPermission
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from .serializers import CourseSerializer, AccountBlockSerializer, BlockSerializer, TeamSerializer
from .models import Course, AccountRole, AccountCourse, Block, AccountBlock, Team, AccountTeam


class CourseListViewSet(ApplicationViewSet, ListAPIView):
    serializer_class = CourseSerializer

    def get_queryset(self):
        account_courses = AccountCourse.objects.filter(account=self.request.user)
        return Course.objects.filter(id__in=[account_course.course.id for account_course in account_courses])

    @action(detail=False, methods=['post'])
    def member(self, request, *args, **kwargs):
        account_course = AccountCourse.objects.filter(course=self.request.data.get('course', 0), account=request.user)

        if account_course.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST)

        course = get_object_or_404(Course, id=self.request.data.get('course', 0))
        AccountCourse.objects.create(
            course=course,
            account=request.user,
            role=self.request.data.get('role', '')
        )

        return Response(status=status.HTTP_204_NO_CONTENT)


class CourseViewSet(ViewSet, RetrieveUpdateAPIView):
    serializer_class = CourseSerializer
    permission_classes = [CoursePermission]
    pagination_class = None
    queryset = Course.objects.all()

    def update(self, request, *args, **kwargs):
        if request.account_course.role == AccountRole.STUDENT:
            return Response({'message': 'У вас нет прав на редактирование курса.'},
                            status=status.HTTP_400_BAD_REQUEST)

        return super().update(request, *args, **kwargs)


class BlockViewSet(ApplicationViewSet):
    permission_classes = [AssistantPermission]
    serializer_class = BlockSerializer
    queryset = Block.objects.all()


class AccountBlockViewSet(ApplicationViewSet):
    permission_classes = [CoursePermission]
    serializer_class = AccountBlockSerializer

    def get_queryset(self):
        block_id = self.request.data.get('block', None)
        block_id = block_id if block_id else self.request.GET.get('block', None)
        block = get_object_or_404(Block, id=int(block_id))

        account_course = get_object_or_404(AccountCourse, course=block.course, account=self.request.user)

        if account_course.role == AccountRole.STUDENT:
            answers = AccountBlock.objects.filter(block=block, account=self.request.user)

            if not answers.exists():
                account_team = AccountTeam.objects.filter(team__course=block.course, account=self.request.user)

                if account_team.exists():
                    members = [member.account.id for member in AccountTeam.objects.filter(team=account_team[0].team)]
                    answers = AccountBlock.objects.filter(block=block, account__in=members)

            return answers

        return AccountBlock.objects.filter(block=block)


class TeamViewSet(ApplicationViewSet):
    permission_classes = [CoursePermission]
    serializer_class = TeamSerializer

    @action(detail=False, methods=['post'])
    def member(self, request, *args, **kwargs):
        team = get_object_or_404(Team, id=self.request.data.get('team', 0))

        account_team = AccountTeam.objects.filter(team=team, account=self.request.user)

        if account_team.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST)

        account_course = get_object_or_404(AccountCourse, course=team.course, account=self.request.user)

        if not account_course.role == 'Student':
            return Response(status=status.HTTP_400_BAD_REQUEST)

        AccountTeam.objects.create(
            team=team,
            account=request.user,
        )

        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_queryset(self):
        course_id = self.request.GET.get('course', 0)
        course = get_object_or_404(Course, id=int(course_id))
        return Team.objects.filter(course=course)

