from django.shortcuts import get_object_or_404
from rest_framework import serializers

from accounts.serializers import UserCustomSerializer
from .models import Course, AccountBlock, Block, AccountCourse, AccountRole, Team, AccountTeam
from django.contrib.auth import get_user_model

User = get_user_model()


class BlockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Block
        fields = '__all__'


class AccountCourseSerializer(serializers.ModelSerializer):
    team = serializers.SerializerMethodField(read_only=True)

    def get_team(self, obj):
        team = AccountTeam.objects.filter(team__course=obj.course, account=self.context['request'].user)

        if not team.exists():
            return None

        return TeamSerializer(instance=team[0].team, context=self.context).data

    def create(self, validated_data):
        course_id = self.context['request'].data.get('course')
        course = get_object_or_404(Course, id=course_id)
        return AccountCourse.objects.create(course=course, account=self.context['request'].user, **validated_data)

    class Meta:
        model = AccountCourse
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    blocks = serializers.SerializerMethodField(read_only=True)
    account_course = serializers.SerializerMethodField(read_only=True)

    def get_blocks(self, obj):
        blocks = Block.objects.filter(course=obj)

        account_course = get_object_or_404(AccountCourse, course=obj, account=self.context['request'].user)

        if account_course.role == AccountRole.STUDENT:
            blocks = blocks.filter(status__in=[2, 3, 4])

        return BlockSerializer(instance=blocks, many=True, context=self.context).data

    def get_account_course(self, obj):
        account_course = AccountCourse.objects.get(course=obj, account=self.context['request'].user)
        return AccountCourseSerializer(instance=account_course, context=self.context).data

    class Meta:
        model = Course
        fields = "__all__"


class AccountBlockSerializer(serializers.ModelSerializer):
    account = serializers.SerializerMethodField(read_only=True)
    assistant = serializers.SerializerMethodField(read_only=True)
    team = serializers.SerializerMethodField(read_only=True)

    def get_team(self, obj):
        team = AccountTeam.objects.filter(account=obj.account, team__course=obj.block.course)

        if not team.exists():
            return None
        return TeamSerializer(instance=team[0].team, context=self.context).data

    def get_assistant(self, obj):
        if not obj.assistant:
            return None

        return UserCustomSerializer(instance=obj.assistant, context=self.context).data

    def get_account(self, obj):
        return UserCustomSerializer(instance=obj.account, context=self.context).data

    def create(self, validated_data):
        block = validated_data['block']
        answer = AccountBlock.objects.filter(account=self.context['request'].user, block=block)

        if answer.exists():
            answer[0].delete()

        account_team = AccountTeam.objects.filter(team__course=block.course, account=self.context['request'].user)

        if account_team.exists():
            members = [member.account.id for member in AccountTeam.objects.filter(team=account_team[0].team)]
            answers = AccountBlock.objects.filter(block=block, account__in=members)

            if answers.exists():
                answers[0].delete()

        return AccountBlock.objects.create(account=self.context['request'].user,  **validated_data)

    def update(self, instance, validated_data):
        request = self.context['request']
        instance = super().update(instance, validated_data)

        instance.assistant = request.user
        instance.save()

        return instance

    class Meta:
        model = AccountBlock
        fields = "__all__"


class TeamSerializer(serializers.ModelSerializer):
    members = serializers.SerializerMethodField(read_only=True)

    def create(self, validated_data):
        instance = super().create(validated_data)

        AccountTeam.objects.create(team=instance, account=self.context['request'].user)
        return instance

    def get_members(self, obj):
        members = AccountTeam.objects.filter(team=obj)
        return UserCustomSerializer(instance=[member.account for member in members], many=True, context=self.context).data

    class Meta:
        model = Team
        fields = "__all__"
