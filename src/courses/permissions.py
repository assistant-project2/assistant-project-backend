from rest_framework.permissions import IsAuthenticated, SAFE_METHODS
from .models import AccountRole, AccountCourse


class CoursePermission(IsAuthenticated):
    def has_permission(self, request, view):
        course_id = request.data.get('course', None)
        course_id = course_id if course_id else request.GET.get('course', None)

        if not course_id:
            return False

        account_course = AccountCourse.objects.filter(course__id=int(course_id), account=request.user)

        if not account_course.exists():
            return False

        account_course = account_course[0]

        return (
            super().has_permission(request, view) and
            account_course and
            account_course.account == request.user
        )


class SupervisorPermission(CoursePermission):
    def has_permission(self, request, view):
        course_id = request.data.get('course', None)
        course_id = course_id if course_id else request.GET.get('course', None)

        if not course_id:
            return False

        account_course = AccountCourse.objects.filter(course__id=int(course_id), account=request.user)

        if not account_course.exists():
            return False

        account_course = account_course[0]

        return (
            super().has_permission(request, view) and
            account_course.role == AccountRole.SUPERVISOR
        )


class AssistantPermission(CoursePermission):
    def has_permission(self, request, view):
        course_id = request.data.get('course', None)
        course_id = course_id if course_id else request.GET.get('course', None)

        if not course_id:
            return False

        account_course = AccountCourse.objects.filter(course__id=int(course_id), account=request.user)

        if not account_course.exists():
            return False

        account_course = account_course[0]

        return (
            super().has_permission(request, view) and
            account_course.role in (AccountRole.SUPERVISOR, AccountRole.ASSISTANT)
        )


class ReadOnlyPermission(CoursePermission):
    def has_permission(self, request, view):
        return (
            super().has_permission(request, view) and
            request.method in SAFE_METHODS
        )
