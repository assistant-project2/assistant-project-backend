from django.urls import path, include
from rest_framework_nested import routers
from .views import CourseViewSet, CourseListViewSet, BlockViewSet, AccountBlockViewSet, TeamViewSet


router_courses = routers.DefaultRouter()
router_courses.register('course', CourseViewSet, "Course")
router_courses.register('courses', CourseListViewSet, "Courses")
router_courses.register('blocks', BlockViewSet, "Blocks")
router_courses.register('account_blocks', AccountBlockViewSet, "AccountBlocks")
router_courses.register('teams', TeamViewSet, "Teams")

urlpatterns = [
    path('', include(router_courses.urls))
]
