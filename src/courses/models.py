from django.db import models


class Course(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    subtitle = models.TextField()
    image = models.ImageField(upload_to='courses', blank=True, null=True)

    def __str__(self):
        return self.title


class AccountRole(models.TextChoices):
    SUPERVISOR = 'Supervisor', 'Руководитель'
    ASSISTANT = 'Assistant', 'Учебный ассистент'
    STUDENT = 'Student', 'Студент'


class AccountCourse(models.Model):
    account = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    role = models.CharField(max_length=31, choices=AccountRole.choices)

    def __str__(self):
        return f'{self.account} - {self.role} | ({self.id})'


class Block(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    status = models.IntegerField(default=1)
    file = models.FileField(upload_to='blocks', blank=True, null=True)


class AccountBlock(models.Model):
    account = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    block = models.ForeignKey(Block, on_delete=models.CASCADE)
    file = models.FileField(upload_to='blocks', blank=True, null=True)
    score = models.IntegerField(blank=True, null=True)
    assistant = models.ForeignKey('accounts.User', on_delete=models.CASCADE, null=True, blank=True, related_name='assistant')
    comment = models.TextField(null=True, blank=True)


class Team(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)


class AccountTeam(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    account = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
