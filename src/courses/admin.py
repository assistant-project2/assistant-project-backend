from django.contrib import admin
from .models import Course, AccountCourse, Block, AccountBlock, Team, AccountTeam


admin.site.register(Course)
admin.site.register(AccountCourse)
admin.site.register(Block)
admin.site.register(AccountBlock)
admin.site.register(Team)
admin.site.register(AccountTeam)
