from django.contrib import admin
from .models import Worksheet, Question, Answer


admin.site.register(Worksheet)
admin.site.register(Question)
admin.site.register(Answer)
