from rest_framework import serializers
from django.shortcuts import get_object_or_404
from django.db.models import Q
from .models import Worksheet, Question, Answer
from accounts.models import AccountRole
from positions.models import Position


class QuestionSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()
    title = serializers.CharField(max_length=255, required=True)

    def validate(self, attrs):
        request = self.context['request']
        is_tech = attrs.get('is_tech', False)
        text = attrs.get('text', None)

        if request.user_account.role in (AccountRole.SUPERVISOR, AccountRole.HR):
            if is_tech and text:
                raise serializers.ValidationError('У вас нет прав на заполнение технического вопроса.')

            if is_tech or (not is_tech and text):
                return attrs
            raise serializers.ValidationError('Заполните все обязательные поля.')

        if text:
            return attrs
        raise serializers.ValidationError('Заполните все обязательные поля.')

    def create(self, validated_data):
        request = self.context['request']

        if not validated_data.get('is_tech', False) and request.user_account.role in (AccountRole.SUPERVISOR, AccountRole.HR):
            validated_data['filled'] = True
        return Question.objects.create(**validated_data)

    def update(self, instance, validated_data):
        request = self.context['request']
        instance = super().update(instance, validated_data)

        if instance.is_tech:
            instance.filled = True
            instance.author = request.user_account
            instance.save()

        return instance

    def get_author(self, obj):
        from accounts.serializers import UserCustomSerializer

        if not self.context['request'].user_account:
            return None

        if not obj.author:
            return UserCustomSerializer(instance=obj.worksheet.author.account, context=self.context).data
        return UserCustomSerializer(instance=obj.author.account, context=self.context).data

    class Meta:
        model = Question
        fields = '__all__'


class WorksheetSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()
    position = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()
    answers = serializers.SerializerMethodField()

    def create(self, validated_data):
        position_id = self.context['request'].data.get('position')
        position = get_object_or_404(Position, id=position_id)
        return Worksheet.objects.create(position=position, author=self.context['request'].user_account,
                                        **validated_data)

    def get_questions(self, obj):
        questions = Question.objects.filter(worksheet=obj)

        if self.context['request'].user_account.role in (AccountRole.TECH_MANAGER, AccountRole.TECH_EXPERT):
            questions = questions.filter(Q(author=self.context['request'].user_account) |
                                         (Q(filled=False) & Q(is_tech=True)))

        return QuestionSerializer(instance=questions, many=True, context=self.context).data

    def get_answers(self, obj):
        answers = Answer.objects.filter(question__worksheet=obj)

        if self.context['request'].user_account.role in (AccountRole.TECH_MANAGER, AccountRole.TECH_EXPERT):
            answers = answers.filter(question__author=self.context['request'].user_account)

        return AnswerSerializer(instance=answers, many=True, context=self.context).data

    @staticmethod
    def get_position(obj):
        return {
            'id': obj.position.id,
            'title': obj.position.title,
            'priority': obj.position.priority
        }

    @staticmethod
    def get_author(obj):
        return {
            'id': obj.author.account.id,
            'first_name': obj.author.account.first_name,
            'last_name': obj.author.account.last_name,
            'email': obj.author.account.email
        }

    class Meta:
        model = Worksheet
        fields = '__all__'


class AnswerSerializer(serializers.ModelSerializer):
    candidate = serializers.SerializerMethodField()
    question = serializers.SerializerMethodField()

    def get_candidate(self, obj):
        from accounts.serializers import UserCustomSerializer
        return UserCustomSerializer(instance=obj.candidate, context=self.context).data

    def get_question(self, obj):
        return QuestionSerializer(instance=obj.question, context=self.context).data

    class Meta:
        model = Answer
        fields = '__all__'
