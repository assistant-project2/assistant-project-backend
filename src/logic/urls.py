from django.urls import path, include
from rest_framework_nested import routers
from .views import WorksheetViewSet, QuestionViewSet, QuestionCandidateViewSet, AnswerViewSet


router = routers.DefaultRouter()
router.register('logic', WorksheetViewSet, 'Worksheet')
router.register('questions', QuestionViewSet, 'Question')
router.register('questions_candidate', QuestionCandidateViewSet, 'Question Candidate')
router.register('answers', AnswerViewSet, 'Answer')

urlpatterns = [
    path('', include(router.urls))
]
