from rest_framework.generics import ListAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from rest_framework import status
from django.shortcuts import Http404, get_object_or_404
from helpers.application_viewsets import ApplicationViewSet
from courses.permissions import HRPermission, TechPermission, ReadOnlyPermission

from .serializers import WorksheetSerializer, QuestionSerializer
from .models import Worksheet, Question, Answer
from accounts.models import AccountRole

User = get_user_model()
class WorksheetViewSet(ApplicationViewSet):
    serializer_class = WorksheetSerializer
    permission_classes = [HRPermission | ReadOnlyPermission]

    def get_queryset(self):
        if not self.action == 'list':
            return Worksheet.objects.all()

        if self.request.user_account.role in (AccountRole.HR, AccountRole.SUPERVISOR):
            return Worksheet.objects.filter(author=self.request.user_account)

        tech_questions = Question.objects.filter(author=self.request.user_account)
        return Worksheet.objects.filter(id__in=[tech_question.worksheet.id for tech_question in tech_questions])

    @action(detail=True, methods=['patch', 'put'])
    def open(self, request, *args, **kwargs):
        worksheet = self.get_object()
        worksheet.opened = not worksheet.opened
        worksheet.save()

        return Response(status=status.HTTP_204_NO_CONTENT)


class QuestionViewSet(ApplicationViewSet):
    serializer_class = QuestionSerializer
    permission_classes = [HRPermission | TechPermission]
    queryset = Question.objects.all()

    def create(self, request, *args, **kwargs):
        if request.user_account.role in (AccountRole.TECH_MANAGER, AccountRole.TECH_EXPERT):
            return Response({'message': 'У вас нет прав на созданиe вопроса.'}, status=status.HTTP_400_BAD_REQUEST)

        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if request.user_account.role in (AccountRole.TECH_MANAGER, AccountRole.TECH_EXPERT):
            return Response({'message': 'У вас нет прав на удаление вопроса.'}, status=status.HTTP_400_BAD_REQUEST)

        return super().destroy(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if request.user_account.role in (AccountRole.SUPERVISOR, AccountRole.HR) and self.get_object().is_tech:
            return Response({'message': 'У вас нет прав на заполнение технического вопроса.'},
                            status=status.HTTP_400_BAD_REQUEST)

        if request.user_account.role in (AccountRole.TECH_MANAGER, AccountRole.TECH_EXPERT):
            if not self.get_object().is_tech:
                return Response({'message': 'У вас нет прав на заполнение общего вопроса.'},
                                status=status.HTTP_400_BAD_REQUEST)

            if request.data.get('is_tech', False):
                return Response({'message': 'У вас нет прав на изменение типа вопроса.'},
                                status=status.HTTP_400_BAD_REQUEST)

        return super().update(request, *args, **kwargs)


class QuestionCandidateViewSet(ViewSet, ListAPIView):
    serializer_class = QuestionSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = None

    def get_queryset(self):
        worksheet = get_object_or_404(Worksheet, id=self.request.query_params.get('worksheet'))

        check = Answer.objects.filter(question__worksheet=worksheet, candidate=self.request.user).exists()

        # if not worksheet.opened or check:
        #     raise Http404('Анкета не доступна.')

        return Question.objects.filter(worksheet=worksheet)

    @action(detail=False, methods=['post'])
    def answer(self, request, *args, **kwargs):
        worksheet = get_object_or_404(Worksheet, id=self.request.data.get('params').get('worksheet', None))

        if not worksheet.opened:
            raise Http404('Анкета не доступна.')

        for answer in request.data.get('data', []):
            Answer.objects.create(
                question=Question.objects.get(id=int(answer.get('id', None))),
                candidate=request.user,
                text=answer.get('text', None),
            )

        return Response(status=status.HTTP_204_NO_CONTENT)


class AnswerViewSet(ViewSet, DestroyAPIView):
    permission_classes = [HRPermission]
    queryset = Answer.objects.all()
