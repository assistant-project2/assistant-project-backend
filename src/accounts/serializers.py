from djoser.serializers import UserCreatePasswordRetypeSerializer
from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework.exceptions import ValidationError

User = get_user_model()

class UserCustomSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions', 'password')
        read_only_fields = ('email',)
