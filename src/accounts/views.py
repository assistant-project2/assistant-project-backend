from django.shortcuts import redirect
from django.contrib.auth import get_user_model
from rest_framework.views import APIView

User = get_user_model()


class ActivationRedirect(APIView):
    def get(self, request, uid, token):
        return redirect(to=f'http://localhost:3000/auth/activation/{uid}/{token}')
